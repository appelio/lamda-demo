import json
import boto3
from datetime import datetime

def lambda_handler(event, context):
    db = boto3.resource('dynamodb')
    table = db.Table('SmartHome')
    
    dateTime = (datetime.now()).strftime("%Y-%m-%d %H:%M:%S")
    device = event['device']
    temp = event['temp']
    
    try:
        table.put_item(
           Item={
                'date': dateTime,
                'device': device,
                'temp': temp
            }
        )
        return {
            'statusCode': 200,
            'body': json.dumps('Succesfully inserted temperature!')
        }
    except:
        print('Closing lambda function')
        return {
                'statusCode': 400,
                'body': json.dumps('Error saving the temperature')
        }