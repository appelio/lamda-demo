# AWS Lambda Demo #

Innehåller Python-kod för AWS-lambda-demo.

Smart home skickar temperatur till DynamoDB via API Gateway och Lambda-funktioner

1. Skapa en DynamDB-tabell (SmartHome)
  * date
  * device
  * temp

2. Skapa IAM-policy och IAM-roll 
  * SmartHomePolicy
  * SmartHomeRole

3. Skapa lambda function
  * SmartHome-SetTemp + SetTempTest
  * SmartHome-GetTemp + GetTempTest

4. Skapa API-gateway
  * POST
  * GET

5. Gör request från Insomnia
  * GET
  * POST