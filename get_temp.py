import json
import boto3

def lambda_handler(event, context):
  db = boto3.resource('dynamodb')
  table = db.Table('SmartHome')
  response = table.scan()
  return {
    'statusCode': 200,
    'body': response['Items']
  }